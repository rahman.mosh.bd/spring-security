package com.rms.springsecurity.auth;

import com.rms.springsecurity.config.JwtService;
import com.rms.springsecurity.user.Role;
import com.rms.springsecurity.user.User;
import com.rms.springsecurity.user.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class AuthenticationService {
    private final UserRepository userRepository ;
    private final PasswordEncoder passwordEncoder ;
    private final JwtService jwtService ;
    private final AuthenticationManager authenticationManager ;

    public AuthenticationResponse register(RegisterRequest registerRequest) {
        var user = User
                .builder()
                .firstName(registerRequest.getFirstName())
                .lastName(registerRequest.getLastName())
                .email(registerRequest.getEmail())
                .password(passwordEncoder.encode(registerRequest.getPassword()))
                .role(Role.USER)
                .build() ;
        userRepository.save(user) ;
        var jwtToken = jwtService.generateJwtToken(user) ;
        return AuthenticationResponse
                .builder()
                .token(jwtToken)
                .build();
    }

    public AuthenticationResponse authenticate(AuthenticationRequest authenticationRequest) {
        authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(
                authenticationRequest.getEmail(),
                authenticationRequest.getPassword()
        ));
       var user = userRepository.findByEmail(authenticationRequest.getEmail()).orElseThrow();
       var jwtToken = jwtService.generateJwtToken(user) ;
        return AuthenticationResponse
                .builder()
                .token(jwtToken)
                .build();
    }
}
