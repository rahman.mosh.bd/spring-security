package com.rms.springsecurity.user;

public enum Role {
    USER,
    ADMIN
}
